This plugin fetches a BOFH Excuses file from a web server (one excuse per
line), picks a random one, and returns it to the channel when the excuse
command is called.
